Rotors: UAV simulator
_____________________

This is a fork of the RotorS MAV simulator written by ASL, ETH Zurich; that adds additional support for sensors and sensor control interfaces such as RGB cameras, 2D LIDAR etc. RGBD support is in the works.

Original code licensed to ASL, ETH Zurich under the Apache License 2.0.

Official repository: https://github.com/ethz-asl/rotors_simulator/wiki
